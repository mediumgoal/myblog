# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse
from .models import Article

def index(request):
    articles = Article.objects.all()
    return render(request,'index.html',{"articles":articles})

